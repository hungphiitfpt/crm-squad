({
    init : function(component, event, helper) {
        var action = component.get("c.getUsers");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.users", response.getReturnValue());
            }
            else {
                console.log("Error fetching users: " + state);
            }
        });
        $A.enqueueAction(action);
    },
     updateOutput : function(component, event, helper) {
        var selectedUserId = component.get("v.selectedUserId");
        component.set("v.outputUserId", selectedUserId);
        console.log( selectedUserId);
    }
})