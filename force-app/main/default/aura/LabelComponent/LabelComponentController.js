({
    // Function to initialize component
    doInit : function(component, event, helper) {
        // Define design attributes
        var design = {
            labelText: {
                label: "Label",
                type: "String"
            },
            value: {
                label: "Value",
                type: "String"
            }
        };
        component.set("v.design", design);
    }
})