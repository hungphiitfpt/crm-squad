({
    doInit : function(component, event, helper) {
     	component.set("v.Message1","Button Initvalue1");
        component.set("v.Message2","Button Initvalue2");
	},
	handleClick : function(component, event, helper) {
        var target = event.getSource();
        console.log(target.get('v.label'));
        component.set("v.Message1",target.get('v.label'));
        component.set("v.Message2",target.get('v.label'));
	}
})