({
	    doInit : function(component, event, helper) {
		var action = component.get("c.show");
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state === "SUCCESS" ){
                component.set('v.accounts',response.getReturnValue());
                console.log(response.getReturnValue());
            } else {
        	console.error(response.getError());
      		}
        });
        $A.enqueueAction(action);
	}
})