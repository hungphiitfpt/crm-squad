({
    doInit : function(component, event, helper) {
        var accountId = component.get("v.accountId");
        var action = component.get("c.getAccountOwner");
        
        action.setParams({
            "accountId": accountId
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var accountOwner = response.getReturnValue();
                component.set("v.accountOwner", accountOwner);
            }
            else {
                console.log("Error retrieving account owner");
            }
        });
        
        $A.enqueueAction(action);
    }
})