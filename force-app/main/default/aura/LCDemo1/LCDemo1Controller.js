({
	doInit : function(component, event, helper) {
		component.set("v.Var1","Demo Value From Component's Controller");
        var data = {'name':'Hung Phi', 'email':'test@gmail.com'};
        component.set("v.jsObject",data);
        component.set("v.userData",
                      {
                          'myString1':'StringValue',
                          'myInteger1':2023
        			  }
                     );
	}
})