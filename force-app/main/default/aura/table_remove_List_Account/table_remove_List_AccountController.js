({
    init : function(component, event, helper) {
        var action = component.get("c.getAccount");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.accounts", response.getReturnValue());
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
    },
    
    selectAll : function(component, event, helper) {
        var selectedHeaderCheck = event.getSource().get("v.value");
        var selectedAccounts = [];
        var allAccounts = component.get("v.accounts");
        for (var i = 0; i < allAccounts.length; i++) {
            allAccounts[i].selected = selectedHeaderCheck;
            selectedAccounts.push(allAccounts[i].Id);
        }
        component.set("v.accounts", allAccounts);
        component.set("v.selectedAccounts", selectedAccounts);
    },
    
    selectRecord : function(component, event, helper) {
    var selectedAccountId = event.getSource().get("v.value");
        console.log('id vừa chọn'+selectedAccountId);
    var selectedAccountIds = component.get("v.selectedAccounts");
    if (selectedAccountIds.includes(selectedAccountId)) {
        selectedAccountIds = selectedAccountIds.filter(function(accountId){ return accountId !== selectedAccountId; });
    } else {
        selectedAccountIds = selectedAccountIds.concat(selectedAccountId);
    }
    component.set("v.selectedAccounts", selectedAccountIds);
},

    
    deleteSelected : function(component, event, helper) {
        var selectedAccountIds = component.get("v.selectedAccounts");
        var action = component.get("c.deleteAccounts");
        action.setParams({
            accountIds: selectedAccountIds
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var accounts = component.get("v.accounts");
                var newAccounts = [];
                accounts.forEach(function(account){
                    if(!selectedAccountIds.includes(account.Id)){
                        newAccounts.push(account);
                        console.log('mảng'+newAccounts);
                    }
                });
                component.set("v.accounts", newAccounts);
                component.set("v.selectedAccounts", []);
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
    }
})