({
	doAction : function(component, event, helper) {
		var selectedDateInput = component.find("selectedDate2");
        var selectedDate = selectedDateInput.get("v.value");
        console.log("Selected Date: ", selectedDate);
        // kiểm tra có phải number không
        if(isNaN(selectedDate)){
            selectedDateInput.set("v.errors",[{
                message: "Input not a number: " + selectedDate
            }]);
        } else {
            selectedDateInput.set("v.errors",null);
        }
	},
    handleDateChange : function(component, event, helper) {
		var selectedDateInput = component.find("selectedDate");
        var selectedDate = selectedDateInput.get("v.value");
        console.log("Selected Date: ", selectedDate);
        // kiểm tra có phải number không
        if(isNaN(selectedDate)){
            selectedDateInput.set("v.errors",[{
                message: "Input not a number: " + selectedDate
            }]);
        } else {
            selectedDate.set("v.errors",null);
        }
	}
})