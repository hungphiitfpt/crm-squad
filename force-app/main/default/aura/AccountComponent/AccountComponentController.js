({
    handleCreateAccount: function(component, event, helper){
        event.preventDefault();
        var accountName = component.get("v.accountName");
        var industry = component.get("v.industry");
        var phone = component.get("v.phone");
        console.log(accountName,industry,phone);
        var action = component.get("c.createAccount");
        var self = this;
        action.setParams({
            "accountName": accountName,
            "industry": industry,
            "phone": phone
        });
        action.setCallback(this, (response)=>{
            var state = response.getState();
            if(state === "SUCCESS"){
                var newAccount = response.getReturnValue();
                component.set('v.accountId', newAccount.Id);
                component.set('v.message',"Account create with Id: "+ newAccount.Id);
                self.doInit(component, event, helper);
            } else {
                component.set("v.message","failed to create account");
            }
        });
        $A.enqueueAction(action);
    },
    doInit : function(component, event, helper) {
		var action = component.get("c.getAccount");
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state === "SUCCESS" ){
                component.set('v.accounts',response.getReturnValue());
                console.log(response.getReturnValue());
            } else {
        	console.error(response.getError());
      		}
        });
        $A.enqueueAction(action);
	}
    
})