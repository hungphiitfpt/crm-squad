({
	myAction : function(component, event, helper) {
        component.set('v.Columns',[
            {label:"Last Name", fieldName:"LastName", type:"text"},
            {label:"Last Name", fieldName:"LastName", type:"text"},
            {label:"Last Name", fieldName:"LastName", type:"text"}
        ]);
        var action = component.get("c.getContacts");
    
        action.setParams({
            recordId: component.get("v.recordId")
        });
        action.setCallback(this, data => {
            component.set("v.Contacts",data.getReturnValue());
        })
        $A.enqueueAction(action);
	}
})