({
	doInit : function (component, event, helper) {
    	helper.getContactRecords(component);
	},
    handleEditOption : function(component, event, helper){
    const recordId = event.getParam("value");
    const editRecordEvent = $A.get("e.force:editRecord");
    editRecordEvent.setParams({
        "recordId": recordId
    });
    editRecordEvent.fire();
}
})