({
	getContactRecords : function(component) {
        // lấy id của account
    	const accId = component.get('v.recordId');
        // gọi đến hàm có tên getContactrecordsList ở controller GetContactForAccountRecords
        const action = component.get('c.getContactrecordsList');
        //set các cột và trả giá trị về cho table
  	    component.set('v.Columns', [
	{label: 'Id', fieldName: 'Id', type: 'url',
    			  typeAttributes: {label: { fieldName: 'Id' }, 
                  iconName: 'utility:preview', 
                  alternativeText: 'View List Contact follow Contact'}},
    {label:"Name", fieldName:"Name", type:"text"},
    {label:"Action", type:"button", 
     				 typeAttributes: {label: 'Edit',name: 'edit',title: 'Click to Edit'}}]);
		action.setParams({
            accId: accId
        });
        // gọi callBack để trả về data sau khi con troller 
        action.setCallback(this, function(response) {
            const state = response.getState();
            if (state === 'SUCCESS') {
                component.set('v.contactRec', response.getReturnValue());
                  console.log( response.getReturnValue());
            }
            const contactRecIds = component.get('v.contactRec');
            console.log('contactIdd is ===== ' + JSON.stringify(component.get('v.contactRec')));
         });
         $A.enqueueAction(action);
    },
})