({
	doInit : function(component, event, helper) {
        var action = component.get("c.getAccount");
        action.setCallback(this, response => {
            var state = response.getState();
            if(state = "SUCCESS"){
            	component.set('v.accounts',response.getReturnValue());
        	}
        });
        $A.enqueueAction(action);
    },
    handleChange : function(component, event, helper) {
        var accountId = document.getElementById("Contacts_List").value;
        var action = component.get("c.getContacts");
        action.setParams({
            "recordId":  accountId
        });
        action.setCallback(this, response => {
            var state = response.getState();
            if(state = "SUCCESS") {
            component.set('v.contacts',response.getReturnValue());
            if(response.getReturnValue().length == 0) {
            console.log(response.getReturnValue())
            alert("Account You're selected is not find data Contacts");
        }
        }
        });
        $A.enqueueAction(action);
    },
})