public with sharing class UserListController {
    @AuraEnabled
    public static List<User> getUsers() {
        return [SELECT Id, Name, SmallPhotoUrl FROM User];
    }

}