public class OrderEventTriggerHandler {
    public static void ordertaskevent(list<Order_Event__e> olist){
        if(!olist.isEmpty()){
            List<Task> listTask = new List<Task>();
            for(Order_Event__e event : olist) {
                if(event.Has_Shipped__c  == true) {
                    Task tlist = new Task();
                    tlist.Priority = 'Medium';
                    tlist.Subject = 'Follow up on shipped order 105';
                    tlist.OwnerId = event.CreatedById;
                    listTask.add(tlist);
                }
            }
            insert listTask;
        }
    }
}