public class AccountControllerShowFieldAccountOwner {
	@AuraEnabled
    public static String getAccountOwner(String accountId) {
        Account acc = [SELECT Owner.Name FROM Account WHERE Id = :accountId];
        return acc.Owner.Name;
    }
}