public class OpportunityContact {
        @InvocableVariable(required=true)
        public Id opportunityId;
        @InvocableVariable(required=true)
        public Id contactId;
        @InvocableVariable(required=true)
        public String role;
}