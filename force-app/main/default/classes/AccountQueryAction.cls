public class AccountQueryAction {

    @InvocableMethod(label='Get Account Names')
    public static List<String> getAccountName(List<Id> ids){
        // phi commit task 1112
        List<String> accountNames = new List<String>();
        List<Account> accounts = [SELECT Name from Account where Id in :ids];
            for(Account account:accounts){
				accountNames.add(account.Name);
            }
        return accountNames;
    }
}