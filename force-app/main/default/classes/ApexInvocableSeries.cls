public class ApexInvocableSeries {
	@InvocableMethod(label='Delete Related Opportunities'
                     description='Delete Related Opportunities for the given Record Ids'
                     category='Account')
    public static void deleteOpportunities(List<String> ids){
        // task 113
        List<Opportunity> oopList = [SELECT Id FROM Opportunity where AccountId =: ids and StageName= 'Closed Won'];
        delete oopList;
    }
}