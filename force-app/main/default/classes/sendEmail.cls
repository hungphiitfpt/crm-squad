public class sendEmail {
	    public static void SendEmailTemplateWithoutTemplate(List<String> address) {
        List<Messaging.SingleEmailMessage> emailList = new List<Messaging.SingleEmailMessage>();
            
       	Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setSubject('HIC Global Solutions');
        mail.setplaintextbody('Welcome to HIC Global Solutions');
        mail.setToAddresses(address);
        emailList.add(mail);
        
        if(!emailList.isEmpty()){
            Messaging.sendEmail(emailList);
        }	   
    } 
}