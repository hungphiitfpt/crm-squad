public with sharing class AccountController {

    @AuraEnabled
    public static List<Account> getAccount(){
        List<Account> accList = new List<Account>();
        accList = [SELECT Id, Name,industry,phone FROM Account];
        return accList;
    }
    
    @AuraEnabled
   	public static Account getAccountLimit() {
        // phi edit task 1111
        int a = 2;
      return [SELECT Id, Name, LastModifiedDate FROM Account LIMIT 1];
    }
    
    @AuraEnabled
    public static List<Contact> getContactFromId(){
        List<Contact> accList = new List<Contact>();
            accList = [SELECT Id, Name FROM Contact];
        return accList;
    }
    
    @AuraEnabled
    public static Account createAccount(String accountName, String industry, String phone) {
        Account newAccount = new Account();
        newAccount.Name = accountName;
        newAccount.Industry = industry;
        newAccount.Phone = phone;
		insert newAccount;
        return newAccount;
    }
    
    @AuraEnabled
public static void deleteAccounts(List<String> accountIds) {
    delete [SELECT Id FROM Account WHERE Id IN :accountIds];
}
}