public with sharing class ContactController {
	@AuraEnabled
    public static List<Account> getAccount(){
        return [SELECT Id, Name FROM Account];
    }
    @AuraEnabled
    public static List<Contact> getContacts(Id recordId){
        return [SELECT Id, FirstName, LastName, Email, Phone FROM Contact WHERE AccountId =: recordId];
    }
}