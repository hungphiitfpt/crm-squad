public class GetContactForAccountRecords {
	@AuraEnabled
    public static List<Contact> getContactrecordsList(Id accId){
        List<Contact> newConList = new List<Contact>();
        for(Account acc : [select id,(select Id, Name, Email, Phone from contacts) from Account where id =:accId]){
            for(Contact c : acc.Contacts){
               newConList.add(c); 
            }
        }       
        return newConList;
    }
}